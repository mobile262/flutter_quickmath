import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import 'HomePage.dart';

class YourScorePage0 extends StatelessWidget {
  const YourScorePage0({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SafeArea(
          child: Scaffold(
            backgroundColor: Color(0xFF95A5A6),
            body: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xFF33495C),
                        borderRadius: BorderRadius.circular(15)),
                    width: 232,
                    height: 52,
                    child: Column(children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Your Score",
                        style: TextStyle(
                            color: Color(0xFF5EB703),
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      )
                    ]),
                  ),
                  SizedBox(
                    height: 70,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 35),
                    child: Image.asset(
                      "Picture/pngaaa 2.png",
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xFFFBBC72),
                        borderRadius: BorderRadius.circular(15)),
                    width: 129,
                    height: 138,
                    child: Column(children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "0",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 100,
                            fontWeight: FontWeight.bold),
                      )
                    ]),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: 129,
                    height: 138,
                    child: Column(children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "YOUR ARE MONKEY!!",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 27,
                            fontWeight: FontWeight.bold),
                      ),
                    ]),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 7, right: 4),
                      child: ElevatedButton(
                        child: Text(
                          "MAIN MENU ",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, fontSize: 25),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(200, 55),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                          ),
                          primary: Color(0xFF33495C),
                          onPrimary: Color(0xFF5EB703),
                        ),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return HomePage();
                          }));
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
