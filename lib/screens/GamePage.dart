import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_quickmath/screens/GameOver.dart';
import 'package:flutter_quickmath/screens/HomePage.dart';

import 'dart:math';
import 'dart:async';

import 'package:flutter_quickmath/screens/YourScorePage0.dart';
import 'package:flutter_quickmath/screens/YourScorePage2.dart';
import 'package:flutter_quickmath/screens/YourScorePage3.dart';
import 'package:flutter_quickmath/screens/yourScorePage1.dart';

class GamePage extends StatefulWidget {
  const GamePage({super.key});

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  int num1 = 7;
  int num2 = 9;
  int choice = 1;
  int score = 0;
  int maxChoice = 3;
  int timeCounter = 10;
  late Timer timer;
  List<String> answers = ['2', '10', '15', '16'];

  startTimer() {
    timer = Timer.periodic(Duration(milliseconds: 1000), (_) {
      setState(() {
        timeCounter--;
        if (timeCounter <= 0) {
          // game over
          timer.cancel();
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return GameOver();
              },
            ),
          );
        }
      });
    });
  }

  _GamePageState() {
    startTimer();
  }

  void checkAnswer(number) {
    if (num1 + num2 == int.parse(number)) {
      setState(() {
        score++;
      });
    }
    setState(() {
      choice++;

      Random random = new Random();
      num1 = random.nextInt(11);
      num2 = random.nextInt(11);

      answers = [];
      answers.add("${random.nextInt(11)}");
      answers.add("${random.nextInt(11)}");
      answers.add("${random.nextInt(11)}");
      answers.add("${num1 + num2}");
      answers.shuffle();
      timeCounter = 10;
    });

    if (choice > maxChoice) {
      choice = maxChoice;
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            if (score == 0) {
              return YourScorePage0();
            } else if (score == 1) {
              return YourScorePage1();
            } else if (score == 2) {
              return YourScorePage2();
            } else {
              return YourScorePage3();
            }
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF95A5A6),
        body: Center(
          child: Column(
            children: [
              SizedBox(
                height: 66,
              ),
              Text(
                "START...",
                style: TextStyle(
                    color: Color(0xFFFFFFFF),
                    fontSize: 60,
                    fontWeight: FontWeight.w900),
              ),
              SizedBox(
                height: 51,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Color(0xFF33495C),
                    borderRadius: BorderRadius.circular(15)),
                width: 309,
                height: 61,
                child: Column(children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    " $num1 + $num2 = ?",
                    style: TextStyle(
                        color: Color(0xFF5EB703),
                        fontSize: 40,
                        fontWeight: FontWeight.w900),
                  ),
                ]),
              ),
              SizedBox(
                height: 91,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 60,
                    height: 97,
                  ),
                  GestureDetector(
                    onTap: () => {checkAnswer(answers[0])},
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFFBBC72),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      width: 98,
                      height: 104,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Text(answers[0],
                              style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontWeight: FontWeight.w900,
                                  fontSize: 70))
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 75,
                  ),
                  GestureDetector(
                    onTap: () => {checkAnswer(answers[1])},
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFFBBC72),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      width: 98,
                      height: 104,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Text(answers[1],
                              style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontWeight: FontWeight.w900,
                                  fontSize: 70))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 37,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 60,
                    height: 97,
                  ),
                  GestureDetector(
                    onTap: () => {checkAnswer(answers[2])},
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFFBBC72),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      width: 98,
                      height: 104,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Text(answers[2],
                              style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontWeight: FontWeight.w900,
                                  fontSize: 70))
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 75,
                  ),
                  GestureDetector(
                    onTap: () => {checkAnswer(answers[3])},
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFFBBC72),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      width: 98,
                      height: 104,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Text(answers[3],
                              style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontWeight: FontWeight.w900,
                                  fontSize: 70))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF33495C),
                ),
                width: 392,
                height: 61,
                child: Row(
                  children: [
                    SizedBox(width: 20),
                    Text(
                      "TIMER : $timeCounter S",
                      style: TextStyle(
                          color: Color(0xFFFFFFFF),
                          fontSize: 20,
                          fontWeight: FontWeight.w900),
                    ),
                    SizedBox(
                      height: 5,
                      width: 100,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      width: 116,
                      height: 46,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5,
                          ),
                          Text("$choice / $maxChoice",
                              style: TextStyle(
                                  color: Color(0xFF76C85D),
                                  fontWeight: FontWeight.w900,
                                  fontSize: 30))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
