import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_quickmath/screens/GamePage.dart';
import 'package:flutter_quickmath/screens/MakeBy.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SafeArea(
          child: Scaffold(
            backgroundColor: Color(0xFF95A5A6),
            body: Center(
              child: Column(
                children: [
                  Image.asset("Picture/Icon.png"),
                  Container(
                    decoration: BoxDecoration(
                      color: Color(0xFFFFFFFF),
                    ),
                    width: 400,
                    height: 90,
                    child: Column(children: [
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "QUICK MATH",
                        style: TextStyle(
                            color: Color(0xFF5EB703),
                            fontSize: 50,
                            fontWeight: FontWeight.w900),
                      ),
                    ]),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8, right: 4),
                      child: ElevatedButton(
                        child: Text(
                          "START !!!",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, fontSize: 25),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(299, 52),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                          ),
                          primary: Color(0xFF33495C),
                          onPrimary: Color(0xFF5EB703),
                        ),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return GamePage();
                          }));
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 150,
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8, right: 4),
                      child: ElevatedButton(
                        child: Text(
                          "Make By",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, fontSize: 25),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(299, 52),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                          ),
                          primary: Color(0xFF33495C),
                          onPrimary: Color(0xFF5EB703),
                        ),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return MakeBy();
                          }));
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
