import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_quickmath/screens/HomePage.dart';

class MakeBy extends StatelessWidget {
  const MakeBy({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SafeArea(
          child: Scaffold(
            backgroundColor: Color(0xFF95A5A6),
            body: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xFF33495C),
                        borderRadius: BorderRadius.circular(15)),
                    width: 300,
                    height: 52,
                    child: Column(children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "62160242",
                        style: TextStyle(
                            color: Color(0xFF5EB703),
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      )
                    ]),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xFF33495C),
                        borderRadius: BorderRadius.circular(15)),
                    width: 300,
                    height: 52,
                    child: Column(children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "62160250",
                        style: TextStyle(
                            color: Color(0xFF5EB703),
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      )
                    ]),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xFF33495C),
                        borderRadius: BorderRadius.circular(15)),
                    width: 300,
                    height: 52,
                    child: Column(children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "62160303",
                        style: TextStyle(
                            color: Color(0xFF5EB703),
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      )
                    ]),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Image.asset(
                    "Picture/pngaaa.com-445778.png",
                    width: 250,
                    height: 180,
                  ),
                  SizedBox(
                    height: 70,
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 7, right: 4),
                      child: ElevatedButton(
                        child: Text(
                          "Back",
                          style: TextStyle(
                              fontWeight: FontWeight.w900, fontSize: 25),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(150, 55),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(15),
                            ),
                          ),
                          primary: Color(0xFF33495C),
                          onPrimary: Color(0xFF5EB703),
                        ),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return HomePage();
                          }));
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
