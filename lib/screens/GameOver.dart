import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_quickmath/screens/GamePage.dart';
import 'package:flutter_quickmath/screens/HomePage.dart';

class GameOver extends StatelessWidget {
  const GameOver({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF95A5A6),
        body: Center(
          child: Column(
            children: [
              SizedBox(
                height: 84,
              ),
              Text(
                "GAME",
                style: TextStyle(
                    color: Color(0xFFFFFFFF),
                    fontSize: 50,
                    fontWeight: FontWeight.w900),
              ),
              Text(
                "OVER...",
                style: TextStyle(
                    color: Color(0xFFFFFFFF),
                    fontSize: 50,
                    fontWeight: FontWeight.w900),
              ),
              SizedBox(
                height: 60,
              ),
              Text(
                "T_T",
                style: TextStyle(
                    color: Color(0xFFFFFFFF),
                    fontSize: 50,
                    fontWeight: FontWeight.w900),
              ),
              SizedBox(
                height: 100,
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8, right: 4),
                  child: ElevatedButton(
                    child: Text(
                      "PLAY AGAIN",
                      style:
                          TextStyle(fontWeight: FontWeight.w900, fontSize: 25),
                    ),
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(182, 57),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(15),
                        ),
                      ),
                      primary: Color(0xFF33495C),
                      onPrimary: Color(0xFF5EB703),
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return GamePage();
                      }));
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 100,
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8, right: 4),
                  child: ElevatedButton(
                    child: Text(
                      "MAIN MENU",
                      style:
                          TextStyle(fontWeight: FontWeight.w900, fontSize: 25),
                    ),
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(182, 57),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(15),
                        ),
                      ),
                      primary: Color(0xFF33495C),
                      onPrimary: Color(0xFF5EB703),
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return HomePage();
                      }));
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
