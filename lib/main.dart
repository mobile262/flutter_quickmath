import 'package:flutter/material.dart';
import 'package:flutter_quickmath/screens/GameOver.dart';
import 'package:flutter_quickmath/screens/GamePage.dart';
import 'package:flutter_quickmath/screens/HomePage.dart';
import 'package:flutter_quickmath/screens/MakeBy.dart';
import 'package:flutter_quickmath/screens/YourScorePage0.dart';
import 'package:flutter_quickmath/screens/YourScorePage2.dart';

import 'screens/YourScorePage3.dart';
import 'screens/yourScorePage1.dart';

import 'dart:math';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          // is not restarted.
          primarySwatch: Colors.blue),
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
